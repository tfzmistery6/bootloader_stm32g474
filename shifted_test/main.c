#include "stm32g474xx.h"

/** 
 * @brief  Timer 7 initialization.
 * @param  none.
 * @return none.
 * ������ ������ �������� ������� ������������ ������� ����������.
 */
void tim7_init(void) {
	RCC -> APB1ENR1 |= RCC_APB1ENR1_TIM7EN; //������� ������������ TIM7
	
	TIM7 -> SR = 0; //������� ������ �������
	
	//�� ������� ������� ������� 16 ���
	//�������� �� (4 + 1) ������ ������� ������ 3.2 ���
	//������������ ������� ���������� �� (199 + 1) ������, ��� = 16 ���
	TIM7 -> PSC = 4; //������������
	TIM7 -> ARR = 199; //����-������ (16 ���)
	//TIM7 -> ARR = 99; //����-������ (32 ���)
	//TIM7 -> ARR = 49; //����-������ (64 ���)
	TIM7 -> DIER |= TIM_DIER_UIE; // �������� ���������� �� ������������
	TIM7 -> CR1 = TIM_CR1_CEN; //������� ������
	
	NVIC_EnableIRQ(TIM7_DAC_IRQn); //�������� ���������� NVIC
}

uint16_t counter;

int main() {
	
	__set_PRIMASK(1); //��������� ����������

	SCB -> VTOR = 0x08003000;//��������� ������ ������� ���������� �� ���������� ������

	__set_PRIMASK(0);//��������� ����������
	
	__enable_irq();
	
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
	
	counter = 0;
	
	//���������� ����
	//PA4
	//PA5
	//PA4 PA5 output
	GPIOA -> MODER &= ~(1 << 9 | 1 << 11);
	GPIOA -> OSPEEDR |= (1 << 8 | 1 << 9 | 1 << 10 | 1 << 11); //very hight speed
	
	tim7_init();
	
	GPIOA -> ODR |= GPIO_ODR_OD4;
	
	while(1);
}

//���������� �� ������������ ������� TIM7 (16 ���)
void TIM7_DAC_IRQHandler (void) {
	TIM7 -> SR &= ~ TIM_SR_UIF; //����� ����� ����������
	
	counter ++;
	
	if (counter >= 8000) {
		counter = 0;
		
		GPIOA -> ODR ^= GPIO_ODR_OD4;
	}
}
