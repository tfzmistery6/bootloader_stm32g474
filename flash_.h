#include "stm32g474xx.h"

void FLASH_Unlock(void);
void FLASH_Lock(void);
_Bool flash_ready(void) ;
_Bool check_EOP(void);
_Bool FLASH_Erase_Page(uint8_t page_number);
_Bool FLASH_Clear_Flags(void);
_Bool FLASH_Program_Double_Word(uint32_t address, uint64_t data);
