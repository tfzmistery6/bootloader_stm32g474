/******************************************************************************
 * @file    main.c
 * @brief   ������� ���� ���������� ��� ������������ ����������� Cortex M4
 * @version v0.12
 * @date    08.07.2022
 * @author  ���������� ������� ��������
 * @note    �� "��� ���" �.������
 ******************************************************************************/

#include "stm32g474xx.h"
#include "usart_.h"
#include "tim_.h"
#include "flash_.h"

/*______________________DATA_______________________*/

#define APPLICATION_ADDRESS 0x08003000 //����� ������ ���������
#define JUMP_ADDRESS 0x08003004 //����� ����� ������ � ���������
#define FLASH_PAGE_SIZE 0x1000 //������ �������� � ������

//����� ����������� �������
#define RX_BUF_SIZE 80
char RX_FLAG_END_LINE = 0; //���� �������� ����� ������
char RXi; //��������� �� �������� ������� �� ������
char RXc; //����� ��� ��������� �������
char RX_BUF[RX_BUF_SIZE] = {'\0'}; //������ �� ������

//����������� ����� �� ��������
#define TX_BUF_SIZE 256
int TX_BUF_empty_space; //��������� ��������� ���� � TX_BUF
int TXi_w; //��������� ���� ������ ���� � ������
int TXi_t; //��������� ������ ���������� ���� � ������
char TX_BUF[TX_BUF_SIZE] = {'\0'}; //����������� ������ ��� �������� ������ �� USART

uint16_t input_param[2]; //����� ���������� ������� �������� � USART1

char buffer_[12]; //����� ��� �����, ������� ���������� ������� � USART1

//������ ��������� ����������
uint16_t boot_step;

#define INIT 1
#define SETTINGS 10
#define DELETING 20
#define JUMP 30
#define PROGRAMMING 40
#define DOWNLOADING 50
#define DOWNLOADING_INIT 51


uint16_t delay_counter; //������� �������� �������� �������� � ��������
_Bool upload_permission; //���������� �� ������� � ������� �������� �� ����������
_Bool download_permission; //���������� �� ������� � ���������� �������� � ����������
_Bool jump_permission; //����������� �� ������ � �������� ���������
_Bool next_flag; //���� �� ��������� ������� ��� ������ ������ �����������


uint32_t r_address; //����� ������
uint64_t r_data_1; //��������� ������
uint64_t r_data_2; //��������� ������
char r_word_1[17]; //������ �������� ������� �����
char r_word_2[17]; //������ �������� ������� �����
char r_addr_word[5]; //������ �������� ������

//���������� ��� �������� ����������� �����
int8_t cracc; //������������� �����
int8_t chksum; //���� ����������� �����
char chksum_word[3]; //������ � ����������� ������

/*____________________PROTOTYPES____________________*/

/*___������������___*/
void clear_RXBuffer(void); //������� ������ �������� ��������
char * utoa_div(uint32_t value, char *buffer); //�������������� uint to ascii ����� %
int is_equal(char *string1, char *string2, int len); //������� ��������� ���� ����� (������ strncmp)
int is_digit(char byte); //������� �������� �������� �� �������� ���� ������ � ascii ���������
int get_param(char *buf); //��������� ���������� �� ������
void USART1_TXBuf_append (char *buffer); //�������� ������ � USART1
void RXBuffer_Handler (void); //���������� ������ �������� �������� ����� ����� CR
void programm_data_Handler (void); //�������������� ���������� RXBuffer ��� �������� ����� �� �����
void ASCII_To_Hex(char *buff, uint8_t pos_1, uint8_t pos_2); //��������������� ASCII to Hex
void Hex_data_To_ASCII(char *word, uint64_t data, uint16_t len); //��������������� ������ ������ � ASCII

int main() {
	
	boot_step = 1; //������ � �������������
	delay_counter = 0;
	upload_permission = 0; //������� ������� ���������
	download_permission = 0; //������� ������ ���������
	jump_permission = 0; //������� ������ � ��������� ��������
	next_flag = 0;
	
	r_address = APPLICATION_ADDRESS;
	
	//������� � ����� ������ ������ 0 (����� ������)
	r_word_1[16] = 0;
	r_word_2[16] = 0;
	r_addr_word[4] = 0;
	chksum_word[2] = 0;
	
	//��������� ������������ ����� D
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIODEN;
	//PD2 PP mode (�������� ���������)
	GPIOD -> MODER &= ~(1 << 5);
	GPIOD -> ODR |= GPIO_ODR_OD2; //�������� �������� ���������
	
	usart1_init();
	tim5_init();
	
	//��������� ����������
	NVIC_EnableIRQ(USART1_IRQn);
	NVIC_EnableIRQ(TIM5_IRQn);
	
	while(1){
		if (jump_permission) {
			void (*p)(void) = (void (*)(void))(*((int*)JUMP_ADDRESS)); //���������� ������� ������ � ���������
			p(); //������� � ���������
		}
	};
}

/*_______________INTERRUPTS HANDLERS_______________*/

//��������������� ������ (50 ��)
void TIM5_IRQHandler (void) {
	TIM5 -> SR &= ~ TIM_SR_UIF; //����� ����� ����������
	
	switch (boot_step){
		
		case INIT:
			USART1_TXBuf_append("Bootloader ready.\n");
			boot_step = SETTINGS;
		break;
		
		case SETTINGS:
			RXBuffer_Handler();
			
			delay_counter++;
			//���� ��� ������� ����� (~0.5 ������) #��� ����� 30 ���
			if (delay_counter >= 1500) {
				boot_step = JUMP; //��������� � ������ � �������� ��������
			}
			
			//���� ��������� ���������� �� ������� ������ �� ��
			if (upload_permission) {
				boot_step = DELETING; //��������� � ������� ������
			}
			
			//���� ���� ���������� �� ������ �������� � ����������
			if (download_permission) {
				boot_step = DOWNLOADING_INIT; //��������� � ������ ������
			}
			
		break;
			
		case DELETING:
			USART1_TXBuf_append("Erasing...\n");
		
			FLASH_Clear_Flags();
			FLASH_Unlock();
			
			for (int i = 3; i<=28; i++) {
				FLASH_Erase_Page(i);
			}
			
			USART1_TXBuf_append("Done.\n");
			GPIOD -> ODR ^= GPIO_ODR_OD2; //����� �������� ����
			
			boot_step = PROGRAMMING;
		break;
			
		case PROGRAMMING:
			
			//RX_FLAG_END_LINE = 1;
			programm_data_Handler();

		break;
		
		case DOWNLOADING_INIT:
			GPIOD -> ODR ^= GPIO_ODR_OD2; //����� �������� ����
			USART1_TXBuf_append(":020000040800F2\n"); //�������� ������� � ���������� �������
			boot_step = DOWNLOADING;
			next_flag = 0;
		break;
		
		case DOWNLOADING:
			
			RXBuffer_Handler();
		
			if (next_flag) {
				
				next_flag = 0;
				
				//������ ������ �� ������
				r_data_1 = *(uint64_t *)r_address;
				r_data_2 = *(uint64_t *)(r_address + 8U);
				
				if (r_data_1 != 0xFFFFFFFFFFFFFFFF && r_data_2 != 0xFFFFFFFFFFFFFFFF) {
				
					Hex_data_To_ASCII(r_word_1, r_data_1, 16);
					Hex_data_To_ASCII(r_word_2, r_data_2, 16);
					Hex_data_To_ASCII(r_addr_word, (r_address & 0xFFFF), 4);
				
					// ������� ����������� �����
					cracc = 0;
					cracc += 0x10 + (r_address & 0xFF) + ((r_address >> 8U) & 0xFF);
					for (int i = 0; i <= 7; i++) {
						cracc += ((r_data_1 >> (8*i)) & 0xFF) + ((r_data_2 >> (8*i))& 0xFF);
					}
					chksum = 0x100 - cracc;
					
					Hex_data_To_ASCII(chksum_word, chksum, 2);
					
					//���������� ����������� ������� ������� � ������ 1
					for (int i = 0; i <=7; i += 2) {
						char char_1;
						char char_2;
						
						char_1 = r_word_1[i];
						char_2 = r_word_1[i+1];
						r_word_1[i] = r_word_1[15 - (i + 1)];
						r_word_1[i+1] = r_word_1[15-i];
						r_word_1[15 - (i + 1)] = char_1;
						r_word_1[15-i] = char_2;
					}
					
					//���������� ����������� ������� ������� � ������ 2
					for (int i = 0; i <=7; i += 2) {
						char char_1;
						char char_2;
						
						char_1 = r_word_2[i];
						char_2 = r_word_2[i+1];
						r_word_2[i] = r_word_2[15 - (i + 1)];
						r_word_2[i+1] = r_word_2[15-i];
						r_word_2[15 - (i + 1)] = char_1;
						r_word_2[15-i] = char_2;
					}
					
					USART1_TXBuf_append(":10");
					USART1_TXBuf_append(r_addr_word);
					USART1_TXBuf_append("00");
					USART1_TXBuf_append(r_word_1);
					USART1_TXBuf_append(r_word_2);
					USART1_TXBuf_append(chksum_word);
					USART1_TXBuf_append("\n");
					
				} else {
					Hex_data_To_ASCII(r_addr_word, (r_address & 0xFFFF), 4);
					USART1_TXBuf_append(r_addr_word);
					USART1_TXBuf_append(" blank sector.\n");
				}
				
				r_address += 0x10; //��������� �� ��������� ������� � ������
				
				if (r_address >= 0x0800FFF0) {
					USART1_TXBuf_append(":00000001FF\n");
					boot_step = JUMP;
				}
			}
			
		break;
			
		case JUMP:
	
			__set_PRIMASK(1); //��������� ����������
			SCB->VTOR = APPLICATION_ADDRESS;//��������� ������ ������� ���������� �� ���������� ������
			__set_PRIMASK(0);//��������� ����������
		
			NVIC_DisableIRQ(TIM5_IRQn);
			NVIC_ClearPendingIRQ(TIM5_IRQn);
			NVIC_DisableIRQ(USART1_IRQn);
			NVIC_ClearPendingIRQ(USART1_IRQn);

			__disable_irq();//��������� ����������
		
			jump_permission = 1; //��������� ������
		
		break;
	}
	
}

//���������� USART1
void USART1_IRQHandler(void) {
	//data is transferred to the shift register
	if (USART1 -> ISR & USART_ISR_TXE) {
		if (TXi_t != TXi_w) {
			USART1 -> TDR = TX_BUF[TXi_t];
			TXi_t ++;
			TX_BUF_empty_space++;
	
			if (TXi_t >= 256){ TXi_t = 0; }
		} else {
			USART1 -> CR1 &= ~USART_ISR_TXE; //��������� ���������� �� ��������
		}
	}
	
	//��������� ��� ��� ������ ���
	if (USART1 -> ISR & USART_ISR_RXNE){
		RXc = USART1 -> RDR;
		RX_BUF[RXi] = RXc;
		RXi++;

		//���� �� ������ �������� �������
		if (RXc != 13) {
			if (RXi > RX_BUF_SIZE-1) {
				clear_RXBuffer();
			}
		} else {
			RX_FLAG_END_LINE = 1;
		}
	}
}

/*____________________FUNCTIONS____________________*/

/*___������������___*/
//������� ������ ��������
void clear_RXBuffer(void) {
	for (RXi=0; RXi<RX_BUF_SIZE; RXi++)
		RX_BUF[RXi] = '\0';
	RXi = 0;
	
	input_param[0] = 0;
	input_param[1] = 0;
}

/**
	* @brief  ������ ������ � ����������� ����� �� �������� ����� USART
	* @param  ������ ������, ��������������� �� ��������
  * @retval None
  */
void USART1_TXBuf_append (char *buffer) {
	//���� � ������ ���� ������
	while (*buffer) {
		TX_BUF[TXi_w] = *buffer++;
		TXi_w++;
		TX_BUF_empty_space--;
		
		if (TXi_w >= 256){ TXi_w = 0; }
	}
	
	USART1 -> CR1 |= (1 << 7); //��������� ���������� �� ��������
	
	if (TXi_t >= 256){ TXi_t = 0; }
}

/**
	* @brief  ������� ����� � ���� �������
	* @param  value - �����, ������� ���������� �������������
	*					*buffer - ����� ������, ���� ������ ���������
  * @retval ���������� ����� � ���������� ������������� ��������������
  */
char * utoa_div(uint32_t value, char *buffer){
   buffer += 11; 
		// 11 ���� ���������� ��� ����������� ������������� 32-� ������� �����
		// �  ������������ ����
   *--buffer = 0;
   do
   {
      *--buffer = value % 10 + '0';
      value /= 10;
   }
   while (value != 0);
   return buffer;
}

/**
	* @brief  ������� ��������� �� ������� ascii �������� �� ���� ������
	* @param  byte - ������ ������
	* @retval ���������� 1, ���� ���� �������� ������
  */
int is_digit(char byte) {
	if ((byte == 0x30) | (byte == 0x31) | (byte == 0x32) | (byte == 0x33) |\
		(byte == 0x34) | (byte == 0x35) | (byte == 0x36) | (byte == 0x37) |\
	(byte == 0x38) | (byte == 0x39)) {
		return 1;
	} else {
		return 0;
	}
};

/**
	* @brief  ������� ��������� ���� ����� (������ strncmp)
	* @param  string1 - ������ ������
	*					string2 - ������ ������
	*					len - ������ ��������� (���������� ����)
	* @retval ���������� 1, ���� ������ �����
  */
int is_equal(char *string1, char *string2, int len) {
	while(len --> 0) {
		if(*string1++ != *string2++) {
			return 0;
		}
	}
	return 1;
};

/**
	* @brief  ������� �������������� ASCII � �������� Hex
	* @param  buff - ����� � �������
	*					pos_1 - ��������� �� ������ ��������������
	*					pos_2 - ��������� �� ����� ��������������
	* @retval void
  */
void ASCII_To_Hex(char *buff, uint8_t pos_1, uint8_t pos_2) {
	uint8_t i;
	
	for(i = pos_1; i <= pos_2; i++) {
		if(buff[i] <= '9' && buff[i] >= '0' ) {
			buff[i] -= 0x30;
		} else {
			buff[i] = buff[i] - 0x41 + 10;
		}	
	}	
}

/**
	* @brief  ������� ��������������� hex ����� � ASCII
	* @param  word - ����� ��������
	*					data - �������� �����, ������� ���� �������������
	*					len - ���������� �������� 
	* @retval void
	* @note 	������ ������� ������������� ��� ������������
	* 				������� �������. � ������ ������ 16 �����.
  */
void Hex_data_To_ASCII(char *word, uint64_t data, uint16_t len) {
	uint8_t r_buff; //����� ��� ��������������
	
	for (int i = 0, j = (len - 1); i <= (len - 1); i++, j--) {
		r_buff = ((data >> (4*i)) & 0xF);
		if (r_buff <= 0x9 && r_buff >= 0x0) {
			r_buff += 0x30;
		} else {
			r_buff += 0x37;
		}
		word[j] = r_buff;
	}
}


/**
	* @brief  ����� � ������ ����� ��� ���������� �������
	* @param  *buf - ����� ������� �� ����
	* @retval ���������� 0 ��� ������������� ������
  */
int get_param(char *buf) {
	uint8_t num_of_params = 2;
	uint8_t i = 0;
	uint8_t pos = 0;
	int value = 0;
	
	//����� �� ������� �������
	while(buf[pos] != ' ') {
		pos++;
		//���� ������ ���� ����� � �� ����� �������
		if (pos >= RX_BUF_SIZE) {
			//������� �� �������
			return 0;
		}
	}
	
	//������� �� ���������� ������� ������ ��������� ���������
	while(buf[++pos] != '\r') {
		if (!(is_digit(buf[pos]) | (buf[pos] == ' '))) {
			USART1_TXBuf_append("Read error!\r");
			clear_RXBuffer();
			return 0;
		}
		
		if (buf[pos] != ' ') {
			value = value*10 + (buf[pos] - 48);
		} else {
			input_param[i] = value;
			i++;
			value = 0;
			if (i > num_of_params -1) {
				USART1_TXBuf_append("Read error!\r");
				clear_RXBuffer();
				return 0;
			}
		}
	}
	
	//������� ��������, ������� ����� ����� ������ ������
	input_param[i] = value;
	
	return 1;
};

//��������� ������ �����
void RXBuffer_Handler (void) {
	//���� ���-�� ���� ������� � ����� �������
	if (RX_FLAG_END_LINE == 1) {
		
		//�������� ��������� � �������
		get_param(RX_BUF);
		
		// Reset END_LINE Flag
		RX_FLAG_END_LINE = 0;
		
		//������� ��� ������ ���������������� ������
		if (is_equal(RX_BUF, "programming\r", 12)) {
			upload_permission = 1;
		}
		
		//������� ��� ������ ���������� �������� � ����������
		if (is_equal(RX_BUF, "downloading\r", 12)) {
			download_permission = 1;
			next_flag = 1;
		}
		
		//������� �� ��������� ������
		if (is_equal(RX_BUF, "next\r", 5)) {
			next_flag = 1;
		}
				
		clear_RXBuffer();
	}
}
void programm_data_Handler (void) {
	
	uint16_t data_type; //��� �������� �������
	uint16_t data_length; //������ �������� ������ � �������
	uint32_t data_address; //����� ������ ������ �� �������
	uint16_t data_cs; //����������� ����� ������ ������
	
	uint64_t desired_data_1 = 0; //������ ����� ������ ��� ������ � ������
	uint64_t desired_data_2 = 0; //������ ����� ������
	
	//���� ���-�� ���� ������� � ����� �������
	if (RX_FLAG_END_LINE == 1) {
		
		if (RX_BUF[0] == ':') {
			ASCII_To_Hex(RX_BUF, 1, 8); //8 ��� ����� : - ���������� � �������
			
			data_length = 2*(RX_BUF[2] + 16*RX_BUF[1]);
			data_address = (0x08000000) + RX_BUF[6] + 16*RX_BUF[5] + 256*RX_BUF[4] \
				+ 4096*RX_BUF[3];
			data_type = RX_BUF[8] + 16*RX_BUF[7];
			ASCII_To_Hex(RX_BUF, (9 + data_length), (9 + data_length + 1)); //�������� ������� ����������� �����
			data_cs = 16*RX_BUF[9 + data_length] + RX_BUF[9 + data_length + 1];
			
			//���� ��� ������ ��� ������
			if (data_type == 0x00) {
				
				//���� ������ ������� ������ 32 ����
				if (data_length < 0x20) {
					for (int i = (9 + data_length); i <= 40 ; i++) {
						RX_BUF[i] = 'F'; //��������� ���������
					}
				}
				
				ASCII_To_Hex(RX_BUF, 9, 40); //����������� ���������� ������
				
				//�������� ������ ������� � 64 ����
				for (int i = 24, j = 0; i > 9; i -= 2, j++) {
					desired_data_1 = (desired_data_1  << 8) | (RX_BUF[i] + 16*RX_BUF[i-1]);
				}
				
				//�������� ������ ������� � 64 ����
				for (int i = 40, j = 0; i > 25; i -= 2, j++) {
					desired_data_2 = (desired_data_2  << 8) | (RX_BUF[i] + 16*RX_BUF[i-1]);
				}
				
				FLASH_Clear_Flags();
				FLASH_Program_Double_Word(data_address, desired_data_1);
				FLASH_Program_Double_Word(data_address + 8U, desired_data_2);
				
				USART1_TXBuf_append("Next.\n");
				
			}
			
			if (data_type == 0x01) {
				USART1_TXBuf_append("Loading is complete.\n");
				boot_step = JUMP;
				FLASH_Lock();
			}
			
			if (data_type == 0x04) {
				USART1_TXBuf_append("No additional address required.\n");
				USART1_TXBuf_append("Next.\n");
			}
			
			if (data_type == 0x05) {
				USART1_TXBuf_append("Function address is not required.\n");
				USART1_TXBuf_append("Next.\n");
			}
			
		}	else {
			USART1_TXBuf_append("Wrong sequence");
		}
		
	// Reset END_LINE Flag
	RX_FLAG_END_LINE = 0;
		
	clear_RXBuffer();
	}
	
}
