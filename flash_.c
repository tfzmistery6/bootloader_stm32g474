#include "stm32g474xx.h"

#define FLASH_KEY1 ((uint32_t)0x45670123)
#define FLASH_KEY2 ((uint32_t)0xCDEF89AB)

void FLASH_Unlock(void)
{
	FLASH->KEYR = FLASH_KEY1;
	FLASH->KEYR = FLASH_KEY2;
}

void FLASH_Lock(void)
{
	FLASH->CR |= FLASH_CR_LOCK;
}

_Bool flash_ready(void) 
{
	return !(FLASH->SR & FLASH_SR_BSY);
}

_Bool check_EOP(void)
{
	if(FLASH->SR & FLASH_SR_EOP)
	{	
		FLASH->SR |= FLASH_SR_EOP;
		return 1;
	}	
	return 0;
}	

_Bool FLASH_Clear_Flags(void) {
	
	while(!flash_ready()); //Ожидаем готовности флеша
	
	FLASH -> SR |= FLASH_SR_PGAERR;
	FLASH -> SR |= FLASH_SR_PGSERR;
	
	return check_EOP();//операция завершена, очищаем флаг
}

_Bool FLASH_Erase_Page(uint8_t page_number) 
{
	while(!flash_ready()); //Ожидаем готовности флеша к записи
	
	FLASH->CR|= FLASH_CR_PER; //Устанавливаем бит стирания страницы
	FLASH->CR &= ~(0xFF << 3); //Очищаем регистр номера страницы
	FLASH->CR|= (page_number << 3); //Задаем номер страницы
	FLASH->CR|= FLASH_CR_STRT; // Запускаем стирание
	while(!flash_ready());  //Ждем пока страница сотрется
	FLASH->CR &= ~FLASH_CR_PER; //Сбрасываем бит стирания одной страницы
	
	return check_EOP();//операция завершена, очищаем флаг
}

//Можно записать только по 64 бита за раз
_Bool FLASH_Program_Double_Word(uint32_t address, uint64_t data)
{
	while(!flash_ready()); //Ожидаем готовности флеша к записи
	
	FLASH->CR |= FLASH_CR_PG; //Разрешаем программирование флеша
	
	*(uint32_t *)address = (uint32_t)data;//Пишем первые 32 бита
	
	__ISB();
	
	*(uint32_t *)(address + 4U) = (uint32_t) (data >> 32U); //Пишем вторые 32 бита
	while(!flash_ready());//Ждем завершения операции
	FLASH->CR &= ~FLASH_CR_PG; //Запрещаем программирование флеша
	return check_EOP();
}
